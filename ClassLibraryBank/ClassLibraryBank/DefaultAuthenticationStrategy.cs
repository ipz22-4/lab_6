﻿namespace ClassLibraryBank
{
    public class DefaultAuthenticationStrategy : IAuthenticationStrategy
    {
        public bool Authenticate(int card, int pin, Account[] accounts, out int user)
        {
            user = -1;
            for (int i = 0; i < accounts.Length; i++)
            {
                if (accounts[i].CardNumber == card && accounts[i].Pin == pin)
                {
                    user = i;
                    return true;
                }
            }
            return false;
        }
    }
}
