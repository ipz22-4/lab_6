﻿namespace ClassLibraryBank
{
    public interface IAuthenticationStrategy
    {
        bool Authenticate(int card, int pin, Account[] accounts, out int user);
    }
}
