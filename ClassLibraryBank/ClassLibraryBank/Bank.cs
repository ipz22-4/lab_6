﻿namespace ClassLibraryBank
{
    public delegate void BankHandler(string message);

    public class Bank
    {
        public delegate void BanksHandler(Bank sender, BanksEventArgs e);
        public event BankHandler Notifications;

        public string BankName { get; private set; }
        public int ListOfATMs { get; set; }

        public Bank(string bankName, int listOfATMs)
        {
            BankName = bankName;
            ListOfATMs = listOfATMs;
        }

        public bool Authenticate(int cardNumber, int pin, Account[] accounts, out int userIndex)
        {
            userIndex = -1;

            for (int i = 0; i < Account.amountUsers; i++)
            {
                if (accounts[i].CardNumber == cardNumber)
                {
                    if (accounts[i].Pin == pin)
                    {
                        userIndex = i;
                        Notifications?.Invoke("Authentication successful!");
                        return true;
                    }
                    else
                    {
                        Notifications?.Invoke("Incorrect data entered! Please try again.");
                        return false;
                    }
                }
            }

            return false;
        }
    }
}