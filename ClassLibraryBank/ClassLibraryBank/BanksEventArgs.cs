﻿namespace ClassLibraryBank
{
   public  class BanksEventArgs
    {
        public string Message { get; }

        public BanksEventArgs(string message)
        {
            Message = message;
        }
    }
}
