﻿namespace ClassLibraryBank
{
    public class BankOperationsEventArgs
    {
        public string Message { get; }
        public float Sum { get; }
        public BankOperationsEventArgs(string message, float sum)
        {
            Message = message;
            Sum = sum;
        }
    }
}
