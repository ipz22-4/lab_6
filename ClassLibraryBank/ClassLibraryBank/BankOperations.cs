﻿/*using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryBank
{
     public class BankOperations 
    {
        public delegate void BankOperationsHandler(BankOperations sender, BankOperationsEventArgs e);
        public event BankOperationsHandler Notify;

        public int Sum { get; private set; }

        public BankOperations(int sum) => Sum = sum;

        public void Put(int sum)
        {
            Sum += sum;
            Notify?.Invoke(this, new BankOperationsEventArgs($"На счет поступило {sum}", sum));
        }
        public void CurrentSum()
        {
           
            
            Notify?.Invoke(this, new BankOperationsEventArgs($"На рахунку: {Sum}", Sum));
        }
        public void Take(int sum)
        {
            if (Sum >= sum)
            {
                Sum -= sum;
                Notify?.Invoke(this, new BankOperationsEventArgs($"Сумма {sum} снята со счета", sum));
            }
            else
            {
                Notify?.Invoke(this, new BankOperationsEventArgs("Недостаточно денег на счете", sum));
            }
        }
    }
}
*/