﻿namespace ClassLibraryBank
{
    public class AutomatedTellerMachine
    {
        public delegate void BankOperationsHandler(AutomatedTellerMachine sender, BankOperationsEventArgs e);
        public event BankOperationsHandler Notify;

        public float Money { get; set; }
        public string IdATMs { get; protected set; }
        public string Address { get; protected set; }

        public AutomatedTellerMachine(float money, string idATMs, string address)
        {
            Money = money;
            IdATMs = idATMs;
            Address = address;
        }

        public void Deposit(float amount, Account[] accounts, int userIndex)
        {
            accounts[userIndex].Balance += amount;
            Money += amount;
            Notify?.Invoke(this, new BankOperationsEventArgs($"Deposited {amount} UAH to the account.", amount));
        }

        public void Withdraw(float amount, Account[] accounts, int userIndex)
        {
            if (Money < amount)
            {
                Notify?.Invoke(this, new BankOperationsEventArgs("Unable to withdraw due to technical issues with the bank.", amount));
                return;
            }

            if (accounts[userIndex].Balance < amount)
            {
                Notify?.Invoke(this, new BankOperationsEventArgs("Insufficient funds in the account.", amount));
                return;
            }

            accounts[userIndex].Balance -= amount;
            Money -= amount;
            Notify?.Invoke(this, new BankOperationsEventArgs($"Withdrew {amount} UAH from the account.", amount));
        }

        public bool TransferToCard(float amount, int targetCardNumber, Account[] accounts, int userIndex)
        {
            if (accounts[userIndex].Balance < amount)
            {
                Notify?.Invoke(this, new BankOperationsEventArgs("Insufficient funds in the account.", amount));
                return false;
            }

            for (int i = 0; i < Account.amountUsers; i++)
            {
                if (accounts[i].CardNumber == targetCardNumber)
                {
                    accounts[i].Balance += amount;
                    accounts[userIndex].Balance -= amount;
                    Notify?.Invoke(this, new BankOperationsEventArgs($"Transferred {amount} UAH to card number {targetCardNumber}.", amount));
                    return true;
                }
            }

            Notify?.Invoke(this, new BankOperationsEventArgs($"Card number {targetCardNumber} not found.", amount));
            return false;
        }
    }
}