﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibraryBank;

namespace ConsoleAppBank
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.InputEncoding = System.Text.Encoding.Unicode;
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;
            Console.Title = "Лабораторна робота №6 Пивоварова Д.В. , ВТ-22-2";

            // Створюємо об'єкти акаунтів
            Account[] accounts =
                {  new Account ("Daria", "Pyvovarova", 1234, 1234, 18200),
                   new Account ("Darina", "Kuzmenko", 4321, 4321, 200),
               };

            // Використання шаблону Singleton для банку
            Bank bank = Bank.Instance;
            bank.SetBankName("MyBank");

            AutomatedTellerMachine MyATM = new AutomatedTellerMachine(10000000000, "#123456789", "вул. Небесної Сотні, 28");

            bank.Notifications += PrintInfo;

        autification:
            int user;
            bool flag = false;

            // Використання шаблону Strategy для автентифікації
            IAuthenticationStrategy authStrategy = new DefaultAuthenticationStrategy();
            do
            {
                Console.WriteLine("Введіть номер карти: ");
                int card = CheckInt();
                Console.WriteLine("Введіть пароль: ");
                int pin = CheckInt();
                flag = bank.Authentication(card, pin, accounts, out user, authStrategy);
                Console.ReadKey();

            } while (!flag);
            Console.Clear();

            MyATM.Notify += DisplayMessage;
            accounts[user].RegisterHandler(PrintInfo);

            int num;
            do
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("\n---------- Головне меню ----------\n");
                Console.ResetColor();
                Console.WriteLine("1. Вивести дані користувача.\n2. Перевірити баланс.\n3. Зняти кошти.\n4. Покласти кошти.\n5. Перевести кошти.\n6. Вийти з акаунту.\n0. Вихід з програми");
                Console.Write("Введіть пункт меню: ");
                num = CheckInt();
                switch (num)
                {
                    case 1: Console.Clear(); accounts[user].PrintInfo(); break;
                    case 2: Console.Clear(); accounts[user].PrintBalance(); break;
                    case 3:
                        Console.Clear();
                        Console.Write("Введіть суму зняття: ");
                        float withdrawalAmount = CheckFloat();
                        MyATM.Withdraw(withdrawalAmount, accounts, user);
                        break;
                    case 4:
                        Console.Clear();
                        Console.Write("Введіть суму поповнення: ");
                        float depositAmount = CheckFloat();
                        MyATM.Deposit(depositAmount, accounts, user);
                        break;
                    case 5:
                        Console.Clear();
                        Console.Write("Введіть суму поповнення картки: ");
                        float transferAmount = CheckFloat();
                        Console.Write("Введіть номер картки: ");
                        int targetCardNumber = CheckInt();
                        MyATM.TransferToCard(transferAmount, targetCardNumber, accounts, user);
                        break;
                    case 6: Console.Clear(); goto autification;
                }
            } while (num != 0);

            void DisplayMessage(AutomatedTellerMachine sender, BankOperationsEventArgs e)
            {
                Console.WriteLine($"Сумма транзакції: {e.Sum} UAN");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }

            void PrintInfo(string message)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine(message);
                Console.ResetColor();
                Console.ReadKey();
            }
        }

        // Метод для перевірки коректності введення чисел з плаваючою точкою
        static float CheckFloat()
        {
            bool f;
            float x;
            do
            {
                f = float.TryParse(Console.ReadLine(), out x);
                Console.ForegroundColor = ConsoleColor.Red;
                if (f == false)
                {
                    Console.WriteLine(" Помилка введення значення. Будь-ласка, повторіть введення ще раз!");
                    Console.ResetColor();
                    Console.Write("Введіть значення ще раз: ");
                }
                Console.ResetColor();
            } while (!f);
            return x;
        }

        // Метод для перевірки коректності введення цілих чисел
        static int CheckInt()
        {
            bool f;
            int x;
            do
            {
                f = int.TryParse(Console.ReadLine(), out x);
                Console.ForegroundColor = ConsoleColor.Red;
                if (f == false)
                {
                    Console.WriteLine(" Помилка введення значення. Будь-ласка, повторіть введення ще раз!");
                    Console.ResetColor();
                    Console.Write("Введіть значення ще раз: ");
                }
                Console.ResetColor();
            } while (!f);
            return x;
        }
    }

    // Шаблон Singleton для класу Bank
    public sealed class Bank
    {
        private static readonly Bank _instance = new Bank();
        public event Action<string> Notifications;
        private string bankName;
        private Bank() { }
        public static Bank Instance => _instance;

        public void SetBankName(string name)
        {
            bankName = name;
        }

        // Метод для автентифікації користувача з використанням стратегії
        public bool Authentication(int card, int pin, Account[] accounts, out int user, IAuthenticationStrategy strategy)
        {
            return strategy.Authenticate(card, pin, accounts, out user);
        }
    }
}
